#!/usr/bin/env python3

import argparse
import re
import logging
import sys
import paho.mqtt.client as mqtt
from datetime import datetime
from time import gmtime, strftime, time
import os
import json
import csv
import socket


from btlewrap import available_backends, BluepyBackend, GatttoolBackend, PygattBackend
from mitemp_bt.mitemp_bt_poller import MiTempBtPoller, \
    MI_TEMPERATURE, MI_HUMIDITY, MI_BATTERY


broker = 'openhab'
port=1883

#model = 'MJ_HT_V1'             # dovrebbe leggerlo dalla risposta "name" del BLE
#mac_addr = 'xx:2d:34:36:be:4a' # dovrebbe leggerlo dalla riga di comando
#mac_addr_nr = re.findall("([0-9a-f]{2}):?",mac_addr.lower())
topic_root = 'Xiaomi'          #
 
#topic_id   = '36be4a'          # dovrebbe leggerlo dagli ultimi tre del MAC, lowercase
#topic_id   = "".join(mac_addr_nr[1:6])

client = mqtt.Client()
client.connect(broker, port)


def valid_mitemp_mac(mac, pat=re.compile(r"[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}")):
    """Check for valid mac adresses."""
    if not pat.match(mac.upper()):
        raise argparse.ArgumentTypeError('The MAC address "{}" seems to be in the wrong format'.format(mac))
    return mac



def poll(args):
    """Poll data from the sensor."""
    backend = _get_backend(args)

    mac_addr = args.mac
    mac_addr_nr = re.findall("([0-9a-f]{2}):?",mac_addr.lower())
    topic_id   = "".join(mac_addr_nr[3:6])
    
    poller      = MiTempBtPoller(args.mac, backend)
    #try :
    #    poller      = MiTempBtPoller(args.mac, backend)
    #except :
    #    print("ERROR: MiTempBtPoller(args.mac, backend) : non riesco a collegarmi con BLE a %1s (%1s)" % (args.mac, args.description))
    #    sys.exit(1)

    try :
        temperatura    = poller.parameter_value(MI_TEMPERATURE)
    except :
        print("ERROR: poller.parameter_value(MI_TEMPERATURE) : non riesco a collegarmi con BLE a %1s (%1s)" % (args.mac, args.description))
        #sys.exit(2)
        
    batteria    = poller.parameter_value(MI_BATTERY)
    #temperatura = poller.parameter_value(MI_TEMPERATURE)
    umidita     = poller.parameter_value(MI_HUMIDITY)

    try :
        modelID     = poller.name()
    except :
        print("ERROR: poller.name() : non riesco a collegarmi con BLE a %1s (%1s)" % (args.mac, args.description))
        #sys.exit(2)
        
    try: model_number  = poller.model_number()      
    except : model_number  = ''
    try: serial_number = poller.serial_number()     
    except : serial_number  = ''
    try: manufacturer  = poller.manufacturer_name() 
    except : manufacturer  = ''
    try: hw_ver      = poller.hardware_version()    
    except : hw_ver  = ''
    fw_ver      = poller.firmware_version()
    if (modelID == 'MJ_HT_V1') :
        model = 'Xiaomi MiJia LCD BLE temperature and humidity'
    else:
        model = 'Xiaomi unknow'
    print("Getting data from Mi Temperature and Humidity Sensor")
    print("description : %1s" % args.description )
    print("MAC: {}".format(mac_addr))
    print("ID: {}".format(topic_id))
    print("HW: {}".format(hw_ver))
    print("FW: {}".format(fw_ver))
    print("Manufacturer name: {}".format(manufacturer))
    print("Name: {}".format(modelID))
    print("Model Number: {}".format(model_number))
    print("Serial Number: {}".format(serial_number))
    print("Battery: {}".format(batteria))
    print("Temperature: {}".format(temperatura))
    print("Humidity: {}".format(umidita))
    unixtime = time()
    mygmtime = gmtime(unixtime)
    mytopic = topic_root + "/" + modelID + "/" + topic_id
    my4json = {'timestamp' : strftime("%d-%m-%Y %H:%M:%S", mygmtime)
            ,'temp'     : temperatura
            ,'humidity' : umidita
            ,'battery'  : batteria
            ,'unixtime' : round(unixtime)
            ,'yyyy' : strftime("%Y",mygmtime)
            ,'mm'   : strftime("%m",mygmtime)
            ,'dd'   : strftime("%d",mygmtime)
            ,'hour' : strftime("%H",mygmtime)
            ,'min'  : strftime("%M",mygmtime)
            ,'sec'  : strftime("%S",mygmtime)
            ,'modelID' : modelID
         #   ,'hardware_version' : hw_ver
            ,'firmware_version' : fw_ver
         #   ,'manufacturer_name': manufacturer
            ,'model' : model
            ,'mac'   : mac_addr            
            ,'gateway' : socket.gethostname()
            ,'script'  : __file__
           # ,'test'    : args.mac
            }
    if args.description != '' : my4json['description'] = args.description
    if model_number != ''     : my4json['model_number'] = model_number
    if serial_number != ''     : my4json['serial_number'] = serial_number
    if hw_ver != ''     : my4json['hardware_version'] = hw_ver
    if manufacturer != ''     : my4json['manufacturer_name'] = manufacturer

    client.publish(topic=mytopic, payload=json.dumps(my4json) ,  qos = 1, retain = True)
    #print(json.dumps(my4json, indent=3))


def _get_backend(args):
    """Extract the backend class from the command line arguments."""
    if args.backend == 'gatttool':
        backend = GatttoolBackend
    elif args.backend == 'bluepy':
        backend = BluepyBackend
    elif args.backend == 'pygatt':
        backend = PygattBackend
    else:
        raise Exception('unknown backend: {}'.format(args.backend))
    return backend


def list_backends(_):
    """List all available backends."""
    backends = [b.__name__ for b in available_backends()]
    print('\n'.join(backends))


def main():
    """Main function.

    Mostly parsing the command line arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--backend', choices=['gatttool', 'bluepy', 'pygatt'], default='gatttool')
    parser.add_argument('-v', '--verbose', action='store_const', const=True)
    subparsers = parser.add_subparsers(help='sub-command help', )

    parser_poll = subparsers.add_parser('poll', help='poll data from a sensor and publish to mqtt broker')
    parser_poll.add_argument('--mac', type=valid_mitemp_mac)
    parser_poll.add_argument('--description',  default='')
    parser_poll.set_defaults(func=poll)

    # parser_scan = subparsers.add_parser('scan', help='scan for devices')
    # parser_scan.set_defaults(func=scan)

    parser_scan = subparsers.add_parser('backends', help='list the available backends')
    parser_scan.set_defaults(func=list_backends)

    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    if not hasattr(args, "func"):
        parser.print_help()
        sys.exit(0)

    args.func(args)


if __name__ == '__main__':
    main()



