#!/usr/bin/python3
from lywsd02 import Lywsd02Client
import paho.mqtt.client as mqtt
from datetime import datetime
from time import gmtime, strftime, time
import os
import json
import random
import socket
import getopt, sys, os


#####################################
def help() :
    print("HELP")


def is_a_mac_addr(mac) :
    risposta = False
    if mac in ('E7:2E:01:70:7C:E9', 'E7:2E:01:21:DC:7D') :
        risposta = True
    else :
        risposta = False

    return(risposta)


#####################################

mac_addr = ''#'E7:2E:01:70:7C:E9'

toMqtt = False
broker = 'openhab'
port=1883

modelID    = 'LYWSD02'
topic_root = 'Xiaomi'          # 
topic      = ''
#topic_id   = '' #'707ce9'          # dovrebbe leggerlo dagli ultimi tre del MAC, lowercase

######################################   
#argv = sys.argv[1:]
try:
    opts, args = getopt.getopt(  sys.argv[1:]  ,"hm:",["help","mac="
    ,'mqtt','broker=','port=','topic_root=','topic='
    ])
except getopt.GetoptError:
    help()
    sys.exit(2)

######################################    
for opt, arg in opts:
    if opt in('-h','--help'):
        help()
        sys.exit()

    elif opt in ("-m", "--mac"):
        if arg in ('sopra','sotto') :
            if   arg == 'sopra' : mac_addr = 'E7:2E:01:70:7C:E9'
            elif arg == 'sotto' : mac_addr = 'E7:2E:01:21:DC:7D'
        elif is_a_mac_addr(arg) : 
            mac_addr = arg
        else :
            print("ERRORE: non riesco ad abbinare un indirizzo mac")
            help()
            sys.exit()


    elif opt in ('--mqtt') : 
        toMqtt = True 
    elif opt in ('--broker') : 
        broker = arg
        toMqtt = True
    elif opt in ('--port') : 
        port = arg
        toMqtt = True
    elif opt in ('--topic_root') : 
        topic_root = arg
        toMqtt = True
    elif opt in ('--topic') : 
        topic = arg
        toMqtt = True

######################################    
if topic == '' :
    if is_a_mac_addr(mac_addr) :
        topic_id = ''.join(mac_addr.lower().split(':')[3:6])
    else :
        help()
        print("ERRORE: mac_addr non valido")
        sys.exit()
else :
    topic_id = topic
######################################    

if is_a_mac_addr(mac_addr) == False :
    help()
    print("ERRORE: mac addr non valid")
    sys.exit()

#####################################
# intanto mi collego e procuro i dati
try :
    ble_client = Lywsd02Client(mac_addr)
    temperatura = ble_client.temperature
    umidita     = ble_client.humidity 
    batteria    = ble_client.battery
except :
    help()
    print("ERRORE: collegamento non riuscito con %1s" % mac_addr)
    sys.exit()
#####################################


if (modelID == 'LYWSD02'):
    model = 'Xiaomi MiJia BLE e-ink clock, temperature and humidity' 
else:
    model = 'Xiaomi unkonw'

print("Getting data from",model)
print("Name: {}".format(modelID))
print("Battery: {}".format(batteria))
print("Temperature: {}".format(temperatura))
print("Humidity: {}".format(umidita))
unixtime = time()
mygmtime = gmtime(unixtime)
mytopic = topic_root + "/" + modelID + "/" + topic_id
my4json = {'timestamp' : strftime("%d-%m-%Y %H:%M:%S", mygmtime)
        ,'temp'     : temperatura
        ,'humidity' : umidita
        ,'battery'  : batteria
        ,'unixtime' : round(unixtime)
        ,'yyyy' : strftime("%Y",mygmtime)
        ,'mm' : strftime("%m",mygmtime)
        ,'dd' : strftime("%d",mygmtime)
        ,'hour' : strftime("%H",mygmtime)
        ,'min' : strftime("%M",mygmtime)
        ,'sec' : strftime("%S",mygmtime)
        ,'modelID' : modelID
        ,'model' : model
        ,'mac' : mac_addr            
        ,'gateway' : socket.gethostname()
        ,'script'  : __file__
        }

if toMqtt :
    mqtt_client = mqtt.Client()
    mqtt_client.connect(broker, port)
    mqtt_client.publish(topic=mytopic, payload=json.dumps(my4json) ,  qos = 1, retain = True)

print(json.dumps(my4json))

# di tanto in tanto metto a punto l'ora esatta
if (random.randrange(0,20) < 0):
    ble_client.time = datetime.now()
